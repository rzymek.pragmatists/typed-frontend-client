package org.springframework.samples.petclinic.system;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

@SpringBootTest
public class GenerateSwaggerTests {

    @Autowired
    private WebApplicationContext context;

    @Test
    public void generateSwagger() throws Exception {
        MockMvc mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
        mockMvc.perform(get("/v2/api-docs").accept(APPLICATION_JSON)).andDo((result) -> {
            Path path = Paths.get("target/swagger.json");
            System.out.println("SWAGGER.JSON: ");
            System.out.println(path.toFile().getAbsolutePath());
            Files.write(path, result.getResponse().getContentAsByteArray());
        });
    }

}
