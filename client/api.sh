#!/bin/bash
set -eu
cd $(dirname "$0")

rm -rf ./package/
cp ../target/swagger.json .
docker run --ulimit nofile=122880:122880 --network host --user $(id -u):$(id -g) --rm -v $PWD:/pwd \
   openapitools/openapi-generator-cli:v4.3.0 generate \
   -i /pwd/swagger.json \
   -g typescript-fetch \
   -c /pwd/config.yml \
   -o /pwd/package/ \
   --skip-validate-spec \

sed -i -e 's/| HTTPQuery }/| Object }/' package/src/runtime.ts

cd package
npm publish
